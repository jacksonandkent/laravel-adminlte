<?php
namespace JacksonAndKent\AdminLTE\Helpers;

class MenuItem
{
    public static function get($lib, $routeName, $icon = "fa-link", $linkName = "", $routeParam = "")
    {

        if(\Route::has($routeName))
        	$routeLink = route($routeName, $routeParam);
        else
        	$routeLink = "";

        if (\Request::route()->getName() == $routeName) {
            $s_active = " active";
        } else {
            $s_active = "";
        }

        if ($icon != "") {
            $htmlIcon = '<i class="fa ' . $icon . '"></i>';
        } else {
            $htmlIcon = '';
        }

        $menuItem = '<li class="' . $s_active . '"><a href="' . $routeLink . '" name="' . $linkName . '">' . $htmlIcon . ' <span>' . $lib . '</span></a></li>';

        return $menuItem;
    }
}
