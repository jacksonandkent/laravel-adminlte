<?php

namespace JacksonAndKent\AdminLTE;

use Illuminate\Support\ServiceProvider;

class JandkAdminLTEServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'admin');

        $this->publishes([
            //__DIR__.'/config/jnkAdmin.php' => config_path('jnkAdmin.php'),
            __DIR__.'/views' => base_path('resources/views/vendor/admin'),
            __DIR__.'/assets' => base_path("resources/assets/vendor/admin"),
            __DIR__.'/public/fonts' => public_path("fonts"),
            __DIR__.'/public/images' => public_path("images"),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/Http/routes.php';

        $this->app->make('JacksonAndKent\AdminLTE\Http\Controllers\AdminController');
    }
}
