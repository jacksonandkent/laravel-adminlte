@extends('admin::masterTemplate')

@section("htmlHeaderPageSpecific")
<!-- Mettre ici les entête HTML propres à la page si nécessaire -->
@endsection

@section("scriptsPageSpecific")
<!-- Mettre ici le javascript propre à la page si nécessaire -->
@endsection

@section("content")
        <section class="content-header">
          <h1>
            Package J&K Admin LTE
            <small>Page de démarrage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Fil</a></li>
            <li class="active">d'Ariane</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-md-6">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-question"></i>

                  <h3 class="box-title">Quoi faire à partir de là ?</h3>
                </div>
                <div class="box-body">
                  <p>Les vues ont été copiées dans <span class="text-aqua">/resources/views/vendor/admin</span>.
                    <br/>
                    Le but est de pouvoir les modifier sans toucher au package.
                    <br/>
                  </p>
                  Ci-dessous une liste de ce qu'il faut faire :
                  <ul>
                    <li>Changer le titre</li>
                    <li>Changer l'en-tête</li>
                    <li>Changer le menu</li>
                    <li>Choisir quoi faire avec le menu de droite</li>
                    <li>Changer le footer</li>
                    <li>Choisir quoi faire avec le Fil d'Ariane</li>
                    <li>Créer les pages nécessaires</li>
                    <li>Installer BootForm : https://github.com/adamwathan/bootforms</li>
                    <li>Utiliser les éléments fournis par adminLTE</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="row">

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">Titre</h3>
                </div>
                <div class="box-body">
                  Pour changer le titre de l'admin, il faut éditer 2 fichiers :
                  <ul> 
                    <li><span class="text-aqua">html_header.blade.php</span>, pour changer le titre de la page,</li>
                    <li><span class="text-aqua">header.blade.php</span>, pour changer le titre affiché en haut du menu.</li>
                  </ul> 
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">En-tête</h3>
                </div>
                <div class="box-body">
                  L'en-tête contient le bouton pour fermer/ouvrir le menu de gauche, les notifications, la déconnexion et le bouton pour ouvrir/fermer le menu de droite.
                  <br/><br/>
                  Pour la modifier, il faut éditer <span class="text-aqua">header.blade.php</span>.
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">Menu</h3>
                </div>
                <div class="box-body">
                  Pour changer le menu il faut éditer <span class="text-aqua">left_sidebar.blade.php</span>.
                </div>
              </div>
            </div>

          </div>

          <div class="row">

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">Menu de droite</h3>
                </div>
                <div class="box-body">
                  Un menu supplémentaire est disponible à droite. Ergonomiquement il permet d'envisager des fonctionnalités masquées comme par exemple une configuration globale.
                  <br/><br/>
                  Le fichier à ouvrir pour l'éditer est <span class="text-aqua">right_sidebar.blade.php</span>. 
                  <br/><br/>
                  Le fichier à éditer pour la masquer est <span class="text-aqua">masterTemplate.blade.php</span> (en retirer l'include).
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">Création de page</h3>
                </div>
                <div class="box-body">
                  Pour créer de nouvelles pages, copier/coller ce script (<span class="text-aqua">starter.blade.php</span>) dans le répertoire jacksonandkent/adminlte et créer les routes/controller nécessaires pour afficher la vue.
                  <br/><br/>
                  Les vues de ce répertoire doivent être préfixées par "admin::". Par exemple, cette vue est appellée grâce à <span class="text-aqua">return view("admin::starter");</span>.
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-code"></i>

                  <h3 class="box-title">AdminLTE</h3>
                </div>
                <div class="box-body">
                  L'intérêt de se baser sur AdminLTE est que beaucoup d'éléments sont prévus et il suffit souvent de recopier le html, et parfois d'ajouter une lib JS, pour arriver à un résultat très ergonomique.
                  <br/><br/>
                  La bonne manière de faire est d'aller sur leur page de démo en ligne :
                  <br/>
                  <a href='https://almsaeedstudio.com/preview' target='_blank' class="text-aqua">https://almsaeedstudio.com/preview</a>
                  <br/><br/>
                  Il faut ensuite naviguer dans leur menu de gauche pour voir ce qui existe et préférer un affichage des sources (CTRL+U) à l'inspecteur d'éléments car certain modules sont générés en Javascript (les dataTables par exemple).
                </div>
              </div>
            </div>

          </div>

        </section>
@endsection