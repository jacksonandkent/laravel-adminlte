@section("htmlHeaderPageSpecific")

@endsection

@section("scriptsPageSpecific")

@endsection

<!DOCTYPE html>
<html>
  @include("admin::html_header")
 
  <body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">
      <div class="content-wrapper">

        <style>
            .content-wrapper{
                margin-left: 0px;
            }
            
            #connexion-logo img {
                max-width: 100%;
                height: auto;
                position: relative;
                display: block;
                margin:0 auto;
                margin-bottom: 10px;
            }
        </style>

        <section class="content-header text-center">
          <h1>
            Identification<br/>
            <small>Merci de vous identifier pour accéder à l'administration</small>
          </h1>
        </section>

        <section class="content">
            <div class="row">
                <div style="float:none;margin:0 auto;" class="col-md-4 col-lg-3 row-center">
                    <div id="connexion-logo">
                        <img src="{{ asset('images/admin/login-logo.png') }}">
                    </div>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-lock"></i>
                            <h3 class="box-title">Identifiants</h3>
                        </div>
                        <div class="box-body">
                            @if ($errors->has('identification'))
                            <div id='errorIdent'>{!! $errors->first('identification') !!}</div>
                            @endif

                            {!! BootForm::open()->name("formAdminLogin") !!}
                            {!! BootForm::text('Login', 'login') !!}
                            {!! BootForm::password('Mot de passe', 'password') !!}
                            {!! BootForm::submit('Envoyer')->name('submitBtn') !!}
                            {!! BootForm::close() !!}
                            </div>
                        </div>
                </div>
            </div>
        </section>

      </div>
    </div>

    @include("admin::scripts")
  </body>
</html>