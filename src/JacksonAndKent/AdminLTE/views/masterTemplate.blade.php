<!DOCTYPE html>
<html>
  @include("admin::html_header")
  <!--
  BODY TAG OPTIONS:
  =================
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      @include("admin::header")

      @include("admin::left_sidebar")

      <div class="content-wrapper">
        @yield("content")
      </div>

      @include("admin::footer")

      @include("admin::right_sidebar")
    </div>

    @include("admin::scripts")
  </body>
</html>
