<!DOCTYPE html>
<html>
    <head>
        <title>J&K - Installation de l'admin</title>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: left;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h1>Installer le package d'admin</h1>
                <h2>Instructions</h2>
                <ol>    
                    <li>Executer en ligne de commande "php artisan vendor:publish --provider='JacksonAndKent\AdminLTE\JandkAdminLTEServiceProvider'" pour copier les vues et les assets depuis le package.</li>
                    <li>Ajouter au fichier webpack.mix.js les css et js. (voir plus bas)</li>
                    <li>Lancer npm run dev.</li>
                    <li>Créer une route qui pointe vers la vue starter. (voir plus bas)</li>
                    <li>Rafraichir cette page et suivre les instructions sur la page starter.</li>
                </ol>
                <h2>/webpack.mix.js</h2>
                <p>
    // Admin<br/>
    var adminPath = 'resources/assets/vendor/admin/';<br/>
    mix.styles([<br/>
        adminPath+'css/AdminLTE.min.css',<br/>
        adminPath+'css/all-skins.min.css',<br/>
        adminPath+'css/bootstrap.min.css',<br/>
        adminPath+'css/font-awesome.min.css',<br/>
        adminPath+'css/ionicons.min.css',<br/>
    ], 'public/css/jnk_admin.css');<br/>
    mix.scripts([<br/>
        adminPath+"js/plugins/jQuery/jQuery-2.1.4.min.js",<br/>
        adminPath+"js/bootstrap.min.js",<br/>
        adminPath+"js/app.min.js",<br/>
    ], "public/js/jnk_admin.js");<br/>
                </p>
                <h2>Routage</h2>
                <ol>
                    <li>Faire un fichier routes/admin.php</li>
                    <li>Y ajouter la route vers la vue starter (voir plus bas)</li>
                    <li>Editer /app/Providers/RouteServiceProvider</li>
                    <li>Dans map(), ajouter $this->mapAdminRoutes()</li>
                    <li>Copier/coller la méthode mapWebRoutes() en mapAdminRoutes()</li>
                    <li>Dans le require, remplacer web.php par admin.php</li>
                    <li>Recharger cette page.</li>
                </ol>
                <p>
                    Route::get('/admin', function(){return view("admin::starter");});
                </p>
            </div>
        </div>
    </body>
</html>