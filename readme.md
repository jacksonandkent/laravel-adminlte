# Laravel-AdminLTE
Ce repo contient une version d'AdminLTE qui peut facilement être ajoutée à Laravel.

## Installation

### Ajouter le package au composer.json
Editer /composer.json pour ajouter dans require :

    "jacksonandkent/laravel-adminlte": "dev-master"

Et ajoute un noeud en frère de require :

    "repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/jacksonandkent/laravel-adminlte.git"
        }
    ]

Dire à composer d'installer ce package :

    composer update laravel-adminlte

### Déclarer l'admin dans Laravel
Déclarer le package dans les providers de "config/app.php"

    // AdminLTE par J&K
    JacksonAndKent\AdminLTE\JandkAdminLTEServiceProvider::class,

### Poursuivre l'installation
Aller sur http://urlduprojet/admin et suivre les instructions